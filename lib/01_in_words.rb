  $ones = {
    0 => "zero",
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
  }

  $teens = {
    10 => "ten",
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen",
  }

  $tens = {
    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety",
  }

  $big_nums = {
    100 => "hundred",
    1_000 => "thousand",
    1_000_000 => "million",
    1_000_000_000 => "billion",
    1_000_000_000_000 => "trillion",
  }

class Fixnum
  def in_words
    if self < 10
      return $ones[self]
    elsif self < 20
      return $teens[self]
    elsif self < 100
      return $tens[self] if self % 10 == 0
      return "#{$tens[(self / 10)* 10]} #{$ones[self % 10]}"
    else
      magnitude = size(self)
      num_str = "#{(self / magnitude).in_words} #{$big_nums[magnitude]}"
      if (self % magnitude) != 0
        "#{num_str} #{(self % magnitude).in_words}"
      else
        return num_str
      end
    end
  end

  def size(num)
    if num / 1000 == 0
      return 100
    elsif num / 1_000_000 == 0
      return 1000
    elsif num / 1_000_000_000 == 0
      return 1_000_000
    elsif num / 1_000_000_000_000 == 0
      return 1_000_000_000
    end
    return 1_000_000_000_000
  end

end
